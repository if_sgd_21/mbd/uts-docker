# Ujian Tengah Semester - Manajemen Basis Data

**Nama: Dian Saputra**

**NIM: 1217050037**


Jalankan service/container PostgreSQL dan PGADMIN4 menggunakan teknik kontainerisasi dengan
ketentuan sebagai berikut:
### Ketentuan service pertama (PostgreSQL)
```yml
postgres:
      # Nama kontainer: postgres_container_NAMA
      container_name: postgres_container_dian
      # Postgres versi terbaru, bisa postgres:latest sebenarnya
      # cuma saya pilih yang paling baru dari alpine
      image: postgres:alpine3.18
      environment:
        # Tidak diperkenankan hardcode username dan password pada docker-compose.yml, password disimpan di file .env
        POSTGRES_USER: ${POSTGRES_USER} 
        POSTGRES_PASSWORD: ${POSTGRES_PASSWORD}
        PGDATA: /data/postgres
      volumes:
        - postgres:/data/postgres
      ports:
      # port sebelah kiri untuk host, kanan untuk container
      # jika port host tidak diisi, maka port container akan random
      # Expose port untuk eksternal docker dengan ketentuan 21XXX 
      # (XXX diganti dengan tiga digit terakhir NIM masing-masing)
        - "21037:5432" 
      networks:
        - postgres
      restart: unless-stopped
```
### Ketentuan service kedua (PGADMIN4) kurang lebih sama seperti ketentuan sebelumnya.
```yml
pgadmin:
      container_name: pgadmin_container_dian
      image: dpage/pgadmin4:latest
      environment:
        PGADMIN_DEFAULT_EMAIL: ${PGADMIN_DEFAULT_EMAIL}
        PGADMIN_DEFAULT_PASSWORD: ${PGADMIN_DEFAULT_PASSWORD}
        PGADMIN_CONFIG_SERVER_MODE: ${PGADMIN_CONFIG_SERVER_MODE}
      volumes:
        - pgadmin:/var/lib/pgadmin

      ports:
        # Expose port untuk eksternal docker dengan ketentuan 22XXX 
        # (XXX diganti dengan tiga digit terakhir NIM masing-masing)
        - "${PGADMIN_PORT:-22037}:80"
      networks:
        - postgres
      restart: unless-stopped
```

Setelah setelan konfigurasinya sesuai, lalu deploy aplikasi yang diatur dalam file Docker Compose dengan perintah:
```cmd
docker compose up
```
![ss docker compose up](assets/Screenshot_2023-10-25_083333.png)
Atau jika ingin prosesnya berjalan di latar belakang bisa menggunakan:
```cmd
docker compose up -d
```
![ss docker compose up -d](assets/Screenshot_2023-10-25_132413.png)

PostgreSQL dapat diakses dari internal docker (PGADMIN4) dan eksternal docker
(Dbeaver)

- Akses postgreSQL melalui PGADMIN4:

![](assets/Screenshot_2023-10-25_132925.png)
- Akses postgreSQL melalui DBeaver:

![](assets/Screenshot_2023-10-25_133351.png)

### Buat skema dengan nama salam lalu didalamnya definisikan dan create tabel dengan nama mahasiswas paling tidak terdapat primary key, unique contrainst, dan check constraint.
Buktikan bahwa contrainstnya bekerja.

```cmd
docker exec -it postgres_container_dian psql -U postgres
```

```sql
-- Membuat skema salam
CREATE SCHEMA salam;

-- Menggunakan skema salam
SET search_path TO salam;
```

```sql
-- Membuat tabel mahasiswas
CREATE TABLE mahasiswas (
    id SERIAL PRIMARY KEY,
    nama VARCHAR(100) NOT NULL,
    nim VARCHAR(10) UNIQUE,
    jenis_kelamin VARCHAR(1) CHECK (jenis_kelamin IN ('L', 'P')),
    tanggal_lahir DATE CHECK (tanggal_lahir <= CURRENT_DATE)
);

-- Menambahkan beberapa data ke dalam tabel mahasiswas
INSERT INTO mahasiswas (nama, nim, jenis_kelamin, tanggal_lahir) VALUES
    ('Dian Saputra', '1217050037', 'L', '2002-11-01'),
    ('Bowo Rakabuming', '1217050007', 'L', '2001-02-02');
```
![Result Query](assets/Screenshot_2023-10-25_134206.png)

Cek Constraint:
```sql
INSERT INTO mahasiswas (nama, nim, jenis_kelamin, tanggal_lahir) VALUES
    ('Asep Komarudin', '1217050037', 'A', '2002-11-01');
```
![](assets/Screenshot_2023-10-25_134524.png)
Hasil query error tersebut menunjukkan bahwa constraint yang telah saya buat sebelumnya benar-benar berfungsi. `NIM` memiliki constraint `UNIQUE` sehingga tidak boleh memiliki nilai duplikat. Lalu constraint selanjutnya ada pada kolom `jenis_kelamin` yang hanya mewajibkan 1 dari 2 nilai yaitu `L` atau `P` serta kolom `tanggal_lahir` yang hanya dapat memiliki tanggal yang kurang dari atau sama dengan tanggal sekarang.

### Buat user baru menggunakan SQL script dengan ketentuan:
Nama : backend_dev, Role: CRUD semua table

Nama : bi_dev, Role: hanya read/select semua table/view
```sql
CREATE USER backend_dev LOGIN PASSWORD 'bed';
CREATE USER bi_dev LOGIN PASSWORD 'bid';
```
![ss create user](assets/Screenshot_2023-10-25_140210.png)

Lalu kita beri akses untuk masing-masing user.
```sql
GRANT USAGE ON SCHEMA salam TO backend_dev, bi_dev;
GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA salam TO backend_dev;
GRANT SELECT ON ALL TABLES IN SCHEMA salam TO bi_dev;
```
![ss grant access user](assets/Screenshot_2023-10-25_140418.png)
